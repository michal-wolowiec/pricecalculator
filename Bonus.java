package com.example.PirceCalculator;

public class Bonus {
	private String productName;
	private double promotionPercent;
	private double specialOffer; 
	private double discount; // kwotowy

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getPromotionPercent() {
		return promotionPercent;
	}

	public void setPromotionPercent(double promotionPercent) {
		this.promotionPercent = promotionPercent;
	}

	public double getSpecialOffer() {
		return specialOffer;
	}

	public void setSpecialOffer(double specialOffer) {
		this.specialOffer = specialOffer;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

}
