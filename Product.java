package com.example.PirceCalculator;

public class Product {
	private String name;
	private double price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price, Bonus bon) {
		System.out.println(bon.getDiscount());
		this.price = (price+(price*bon.getPromotionPercent()))-bon.getDiscount();
	}
	

}
