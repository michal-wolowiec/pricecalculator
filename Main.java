package com.example.PirceCalculator;

public class Main {

	public static void main(String[] args) {
		Bonus bon1 = new Bonus();
		Bonus bon2 = new Bonus();
		Product prod1 = new Product();
		Product prod2 = new Product();
		
		 bon1.setProductName("Koszula");
		 bon1.setDiscount(0);
		 bon1.setSpecialOffer(0);
		 bon1.setPromotionPercent(0.30);
		 
		 bon2.setProductName("Spodnie");
		 bon2.setDiscount(20);
		 bon2.setSpecialOffer(0);
		 bon2.setPromotionPercent(0);
		 
		 
		 prod1.setName("Koszula");
		 prod1.setPrice(180, bon1);
		 
		 prod2.setName("Spodnie");
		 prod2.setPrice(250, bon2);
		 
		 System.out.println(prod1.getName());
		 System.out.println(prod1.getPrice());
		 
		 System.out.println("******************");
		 
		 System.out.println(prod2.getName());
		 System.out.println(prod2.getPrice());

	}

}
